class CreateItems < ActiveRecord::Migration
  def change
    create_table :items do |t|
      t.string :name
      t.integer :price
      t.binary :picture
      t.string :category
      t.date :release

      t.timestamps null: false
    end
  end
end
