class CreateComments < ActiveRecord::Migration
  def change
    create_table :comments do |t|
      t.integer :item_id
      t.string :user_name
      t.text :content

      t.timestamps null: false

      t.index :item_id
    end
  end
end
