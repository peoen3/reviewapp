class CreateReviews < ActiveRecord::Migration
  def change
    create_table :reviews do |t|
      t.integer :item_id
      t.string :review
      t.integer :star

      t.timestamps null: false
      t.index :item_id
    end
  end
end
