class AddCategoryIdToItems < ActiveRecord::Migration
  def change
  end
  def self.up
    add_column :item, :category_id, :integer
    add_index :item, :category_id
  end

  def self.down
    remove_index :item, :column => :category_id
    remove_column :item, :category_id
  end
end
