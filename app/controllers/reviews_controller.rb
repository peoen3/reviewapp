class ReviewsController < ApplicationController

  def create
     @item = Item.find(params[:item_id])
     @review = Review.new(params_review)
     @review.item = @item

     if @review.save
       redirect_to item_url(@item)
     else
       render "items/show"
     end
   end

   def destroy
    @item = Item.find(params[:item_id])
    @review = Review.find(params[:id])
    @review.destroy
    redirect_to item_url(@item)
   end

   private

   def params_review
     params.require(:review).permit(:review, :star)
   end

end
