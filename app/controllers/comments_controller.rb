class CommentsController < ApplicationController
  
  def create
     @item = Item.find(params[:item_id])
     @comment = Comment.new(params_comment)
     @comment.item = @item

     if @comment.save
       redirect_to item_url(@item)
     else
       render "items/show"
     end
   end

   def destroy
    @item = Item.find(params[:item_id])
    @comment = Comment.find(params[:id])
    @comment.destroy
    redirect_to item_url(@item)
   end

   private

   def params_comment
     params.require(:comment).permit(:user_name, :content)
   end
end
