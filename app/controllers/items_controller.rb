class ItemsController < ApplicationController
  before_action :set_item, only: [:edit, :update, :destroy]

  def index
    @items = Item.all
    @search = Item.search(params[:q])
    @items = @search.result
  end

  def show
    @item = Item.includes(:comments).find(params[:id])
    @comment = Comment.new
    @item = Item.includes(:reviews).find(params[:id])
    @review = Review.new
  end

  def new
    @item = Item.new
  end

  def create
    @item = Item.new(params_item)
    if @item.save
       redirect_to item_url(@item)
     else
       render "new"
     end
  end

  def edit
  end

  def update
    if @item.update_attributes(params_item)
      redirect_to item_url(@item)
    else
      render "edit"
    end
  end

  def destroy
    @item.destroy
    redirect_to items_url
  end

  def search
    t = Item.arel_table
    name = params[:name]
    @items = Item.all
    @items = @items.where(t[:name].matches("%#{name}%")) if name.present?
  end

  private

  def params_item
    params.require(:item).permit(:name, :price, :picture, :category, :release)
  end

  def set_item
    @item = Item.find(params[:id])
  end

end
