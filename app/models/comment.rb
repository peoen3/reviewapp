class Comment < ActiveRecord::Base
  belongs_to :item

  validates :item, presence: true
  validates :user_name, presence: { message: "名前を入力してください" }
  validates :content, presence: { message: "コメントを入力してください" }, length: { in: 1..1000 }
end
