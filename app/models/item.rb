class Item < ActiveRecord::Base
  has_many :comments, dependent: :destroy
  has_many :reviews, dependent: :destroy

  validates :name, presence: { message: "商品名を入力してください" }
  validates :price,
    presence: { message: "価格を入力してください" },
    numericality: { only_integer: true, greater_than_or_equal_to: 0 }
  mount_uploader :picture, ImageUploader
  belongs_to :category
  
end
