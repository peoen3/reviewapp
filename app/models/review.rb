class Review < ActiveRecord::Base
  belongs_to :item

  validates :item, presence: true
  validates :review, presence: { message: "コメントを入力してください！" }
  validates :star,
  presence: { message: "評価を入力してください！" },
  numericality: {
    less_than: 11,
    only_integer: true,
    greater_than_or_equal_to: 0, # 数値であり、0以上11以下の場合有効
    message: '0~10の整数値で入力してください！'
  }

end
